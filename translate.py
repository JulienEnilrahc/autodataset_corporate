import deepl
import os
import re
import sys

target_lang = sys.argv[1]

translator = deepl.Translator("79df4037-0230-912a-e76b-9a2f25ebbe3b")

html_dir = os.fsencode("src/html")
en_dir = os.path.join(html_dir, os.fsencode("en"))
target_dir = os.path.join(html_dir, os.fsencode(target_lang))
common_dir = os.fsencode("common")
files_dir = os.fsencode("files")

# Text to translate should be like "{% tr Text to translate %}".
regex = re.compile(r'{% tr (.*?) %}', re.S)


def translate(file, dir):
    f_target_read = open(os.path.join(target_dir, dir, file), "r")
    print(f_target_read.name)
    file_content = f_target_read.read()
    f_target_read.close()

    # If lang="en" is already converted, we consider that the whole file
    # was converted so stop here.
    # Works only for files containing this tag (not for common files for example).
    if file_content.find('lang="'+target_lang+'"') != -1:
        print("File already translated, switching to the next file.")
        return

    # Convert lang="en" and header links.
    f_target_write = open(os.path.join(target_dir, dir, file), "w")
    f_target_write.write(file_content.replace(
        'lang="en"', 'lang="'+target_lang+'"').replace(
        "https://autodataset.com/"+file.decode("utf-8"), "https://autodataset.com/"+target_lang+"/"+file.decode("utf-8"))
    )
    f_target_write.close()

    contents_to_translate = regex.findall(file_content)

    for content_to_translate in contents_to_translate:
        print(content_to_translate)
        # Translate with Deepl.
        translated_content = translator.translate_text(
            content_to_translate, target_lang="pt-pt" if target_lang == "pt" else target_lang, source_lang="en"
        )
        print(str(translated_content))

        # Replace translated text in target file, and replace html lang code.
        f_target_read = open(os.path.join(target_dir, dir, file), "r")
        file_content = f_target_read.read()
        f_target_read.close()

        f_target_write = open(os.path.join(target_dir, dir, file), "w")
        f_target_write.write(file_content.replace(
            "{% tr " + content_to_translate + " %}", "{% tr " + str(translated_content) + " %}")
        )
        f_target_write.close()


# Translate all files in files folder.
# for file in os.listdir(os.path.join(en_dir, files_dir)):
#     if file.decode("utf-8") not in ["404.html", "500.html", "503.html",
#                                     "nlpcloud-aide-democratiser-traitement-naturel-du-langage-nlp-en-production.html",
#                                     "nlpcloud-helps-democratize-natural-language-processing-nlp-in-production.html",
#                                     "nlpcloud-supporte-gpt-j-alternative-open-source-gpt-3.html",
#                                     "nlpcloud-supports-gpt-j-open-source-gpt-3-alternative.html",
#                                     "nlpcloud-unterstutzt-gpt-j-open-source-alternative-zu-gpt-3.html",
#                                     "openapi.json",
#                                     "press.html",
#                                     "presse-deutsch.html",
#                                     "presse.html",
#                                     "privacy.html",
#                                     "robots.txt",
#                                     "sla.html",
#                                     "tos.html"]:
#         translate(file, files_dir)

# Translate all files in common folder.
for file in os.listdir(os.path.join(en_dir, common_dir)):
    translate(file, common_dir)

import os
import re
import shutil
from xml.dom import minidom

regex = re.compile(r'{% tr (.*?) %}', re.S)

html_dir = os.fsencode("src/html")
assets_dir = os.fsencode("src/assets")
build_dir = os.fsencode("build")
build_assets_dir = os.fsencode("build/assets")

shutil.rmtree(build_dir)
shutil.copytree(html_dir, build_dir)
shutil.copytree(assets_dir, build_assets_dir)

canonical_urls = {}
for dir in os.listdir(html_dir):
    lang_dir = os.fsencode(os.path.join(build_dir, dir))
    files_dir = os.path.join(lang_dir, os.fsencode("files"))
    common_files_dir = os.path.join(lang_dir, os.fsencode("common"))

    for file in os.listdir(files_dir):
        # {'index.html': ['fr', 'en'], ... }
        canonical_urls.setdefault(file.decode(), []).append(dir.decode())

        f = open(os.path.join(files_dir, file), "r")

        nav = open(os.path.join(common_files_dir,
                                os.fsencode("nav.html")), "r")
        footer = open(os.path.join(common_files_dir,
                                   os.fsencode("footer.html")), "r")

        new_content = f.read().replace("{{ nav }}", nav.read()
                                       ).replace("{{ footer }}", footer.read()
                                                 )

        new_content = regex.sub(r"\1", new_content)

        if dir.decode() == "en":
            f = open(os.path.join(build_dir, file), "w")
            f.write(new_content)

        else:
            f = open(os.path.join(lang_dir, file), "w")
            f.write(new_content)

    shutil.rmtree(common_files_dir)
    shutil.rmtree(files_dir)

en_dir = os.fsencode("build/en")
shutil.rmtree(en_dir)

# Create sitemap.

sitemap_root = minidom.Document()
sitemap_urlset_node = sitemap_root.createElement('urlset')
sitemap_urlset_node.setAttribute(
    'xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9')
sitemap_urlset_node.setAttribute(
    'xmlns:xhtml', 'http://www.w3.org/1999/xhtml')
sitemap_root.appendChild(sitemap_urlset_node)

domain = "https://autodataset.com"

for url, langs in canonical_urls.items():
    # Don't add HTTP errors to sitemap.
    if re.match("\d{3}\.html", url):
        continue

    for lang in langs:
        sitemap_url_node = sitemap_root.createElement('url')
        sitemap_urlset_node.appendChild(sitemap_url_node)
        sitemap_loc_node = sitemap_root.createElement('loc')
        sitemap_url_node.appendChild(sitemap_loc_node)

        if lang == "en":
            sitemap_url = sitemap_root.createTextNode(
                domain + "/" + url)
        else:
            sitemap_url = sitemap_root.createTextNode(domain +
                                                      "/" + lang + "/" + url)

        sitemap_loc_node.appendChild(sitemap_url)

        for lang2 in langs:
            if lang2 != lang:
                sitemap_link_node = sitemap_root.createElement(
                    'xhtml:link')
                sitemap_link_node.setAttribute(
                    'rel', 'alternate')
                sitemap_link_node.setAttribute(
                    'hreflang', lang2)
                if lang2 == "en":
                    sitemap_link_node.setAttribute(
                        'href', domain + "/" + url)
                else:
                    sitemap_link_node.setAttribute(
                        'href', domain + "/" + lang2 + "/" + url)

                sitemap_url_node.appendChild(sitemap_link_node)

xml_pretty = sitemap_root.toprettyxml(encoding="UTF-8", indent="\t")
xml_file = "build/sitemap.xml"
with open(xml_file, "wb") as f:
    f.write(xml_pretty)

## Build

`python build.py`
`docker build -t juliensalinas/ft:autodataset_corporate .`

## Deploy

`docker run -p 80:80 --restart always -d juliensalinas/ft:autodataset_corporate`
